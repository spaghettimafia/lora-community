package com.spaghettimafia.LoRaCommunity.data.repository;

import com.spaghettimafia.LoRaCommunity.data.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {

}
