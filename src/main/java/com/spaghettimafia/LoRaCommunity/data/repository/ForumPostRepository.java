package com.spaghettimafia.LoRaCommunity.data.repository;

import com.spaghettimafia.LoRaCommunity.data.entity.ForumPost;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ForumPostRepository extends JpaRepository<ForumPost, Integer> {

}
