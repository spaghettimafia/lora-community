package com.spaghettimafia.LoRaCommunity.data.repository;

import com.spaghettimafia.LoRaCommunity.data.entity.ForumTopic;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ForumTopicRepository extends JpaRepository<ForumTopic, Integer> {

}
