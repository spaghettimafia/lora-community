package com.spaghettimafia.LoRaCommunity.data.repository;

import com.spaghettimafia.LoRaCommunity.data.entity.WikiArticle;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.Id;

public interface WikiArticleRepository extends JpaRepository<WikiArticle, Integer> {

}
