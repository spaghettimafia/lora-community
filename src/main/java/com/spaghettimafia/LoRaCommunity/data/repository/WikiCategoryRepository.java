package com.spaghettimafia.LoRaCommunity.data.repository;

import com.spaghettimafia.LoRaCommunity.data.entity.WikiCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WikiCategoryRepository extends JpaRepository<WikiCategory, Integer> {

}
