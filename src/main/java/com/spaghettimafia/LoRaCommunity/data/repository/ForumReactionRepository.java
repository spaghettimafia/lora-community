package com.spaghettimafia.LoRaCommunity.data.repository;

import com.spaghettimafia.LoRaCommunity.data.entity.ForumReaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ForumReactionRepository extends JpaRepository<ForumReaction, Integer> {

}
