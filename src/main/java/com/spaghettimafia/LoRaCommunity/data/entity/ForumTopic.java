package com.spaghettimafia.LoRaCommunity.data.entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class ForumTopic {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;
    private String name;

    // Lists
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "topic", cascade = CascadeType.ALL)
    private List<ForumPost> forumPosts;

    // Getters and setters
    public Integer getId() { return id; }

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    public List<ForumPost> getForumPosts() { return forumPosts; }
    public void setForumPosts(List<ForumPost> forumPosts) { this.forumPosts = forumPosts; }

    @Override
    public String toString() {
        return "ForumTopic [id=" + id + ", name=" + name + "]";
    }
}
