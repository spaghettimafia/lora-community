package com.spaghettimafia.LoRaCommunity.data.entity;

import org.ocpsoft.prettytime.PrettyTime;

import javax.persistence.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@Entity
public class ForumReaction {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    private ForumPost post;
    @Column(columnDefinition="text")
    private String content;

    @ManyToOne
    private User user;
    @Column(columnDefinition = "integer default 0")
    private Integer likes = 0;
    private String date;

    // Getters and setters
    public Integer getId() { return id; }

    public ForumPost getPost() { return post; }
    public void setPost(ForumPost post) { this.post = post; }

    public String getContent() { return content; }
    public void setContent(String content) { this.content = content;}

    public User getUser() { return user; }
    public void setUser(User user) { this.user = user; }

    public Integer getLikes() { return likes; }
    public void setLikes(Integer likes) { this.likes = likes; }

    public String getDate() throws ParseException {
        PrettyTime p = new PrettyTime(new Locale("nl"));
        DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
        return p.format(dateFormat.parse(date));
    }
    public void setDate() { this.date = new Date().toString(); }

    @Override
    public String toString() {
        return "ForumReaction [id=" + id + ", user=" + user + "]";
    }
}
