package com.spaghettimafia.LoRaCommunity.data.entity;

import org.ocpsoft.prettytime.PrettyTime;
import org.springframework.data.jpa.repository.Modifying;

import javax.persistence.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@Entity
public class WikiArticle {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    private WikiCategory category;
    private String title;
    @Column(columnDefinition="text")
    private String summary;
    @Column(columnDefinition="text")
    private String content;
    @Column(columnDefinition = "integer default 0")
    private Integer helpful = 0;
    private String date;

    // Getters and setters
    public Integer getId() { return id; }

    public WikiCategory getCategory() { return category; }
    public void setCategory(WikiCategory category) { this.category = category; }

    public String getTitle() { return title; }
    public void setTitle(String title) { this.title = title; }

    public String getSummary() { return summary; }
    public void setSummary(String summary) { this.summary = summary; }

    public String getContent() { return content; }
    public void setContent(String content) { this.content = content;}

    public Integer getHelpful() { return helpful; }
    public void setHelpful(Integer helpful) { this.helpful = helpful; }

    public String getDate() throws ParseException {
        PrettyTime p = new PrettyTime(new Locale("nl"));
        DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
        return p.format(dateFormat.parse(date));
    }
    public void setDate() { this.date = new Date().toString(); }

    @Override
    public String toString() {
        return "WikiArticle [id=" + id + ", title=" + title + "]";
    }
}
