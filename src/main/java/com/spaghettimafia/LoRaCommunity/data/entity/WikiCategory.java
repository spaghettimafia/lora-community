package com.spaghettimafia.LoRaCommunity.data.entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class WikiCategory {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String icon;

    // Lists
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "category", cascade = CascadeType.ALL)
    private List<WikiArticle> wikiArticles;

    // Getters and setters
    public Integer getId() { return id; }

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    public String getIcon() { return icon; }
    public void setIcon() { this.icon = icon; }

    public List<WikiArticle> getWikiArticles() { return wikiArticles; }
    public void setWikiArticles(List<WikiArticle> wikiArticles) { this.wikiArticles = wikiArticles; }

    @Override
    public String toString() {
        return "WikiCategory [id=" + id + ", name=" + name + "]";
    }
}
