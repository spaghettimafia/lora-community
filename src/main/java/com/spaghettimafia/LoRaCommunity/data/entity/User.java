package com.spaghettimafia.LoRaCommunity.data.entity;

import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;

import javax.persistence.*;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Entity
@Table(name="dashboard_users")
public class User {

    @Id
    private String id;
    private String username;
    private String first_name;
    private String last_name;
    private String email;
    private Boolean admin;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.ALL)
    private List<ForumPost> forumPosts;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.ALL)
    private List<ForumReaction> forumReactions;

    public User(){}

    public User(HttpServletRequest request){
        KeycloakAuthenticationToken token = (KeycloakAuthenticationToken) request.getUserPrincipal();
        KeycloakPrincipal principal=(KeycloakPrincipal)token.getPrincipal();
        KeycloakSecurityContext session = principal.getKeycloakSecurityContext();
        AccessToken accessToken = session.getToken();
        username = accessToken.getPreferredUsername();
        id = session.getIdToken().getSubject();
        first_name = accessToken.getGivenName();
        last_name = accessToken.getFamilyName();
        email = accessToken.getEmail();
        if (session.getToken().getRealmAccess().getRoles().contains("admin")) {
            admin = true;
        }
        else {
            admin = false;
        }
    }

    public String getId() { return id; }
    public void setId(String id) { this.id = id; }

    public String getUsername() { return username; }
    public void setUsername(String username) { this.username = username; }

    public String getFirstName() { return first_name; }
    public void setFirstName(String first_name) { this.first_name = first_name; }

    public String getLastName() { return last_name; }
    public void setLastName(String last_name) { this.last_name = last_name; }

    public String getName() { return first_name + ' ' + last_name; }

    public String getEmail() { return email; }
    public void setEmail(String email) { this.email = email; }

    public String getInitials() { return String.valueOf(first_name.charAt(0)) + last_name.charAt(0); }
    
    public Boolean getAdmin() { return this.admin; }
    public void setAdmin(Boolean admin) { this.admin = admin; }

    public List<ForumPost> getForumPosts() { return forumPosts; }
    public void setForumPosts(List<ForumPost> forumPosts) { this.forumPosts = forumPosts; }

    public List<ForumReaction> getForumReactions() { return forumReactions; }
    public void setForumReactions(List<ForumReaction> forumReactions) { this.forumReactions = forumReactions; }
}
