package com.spaghettimafia.LoRaCommunity.service;

import com.spaghettimafia.LoRaCommunity.data.entity.User;
import com.spaghettimafia.LoRaCommunity.data.repository.UserRepository;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class UserService {

    @Autowired
    private UserRepository repository;

    public User getUser(HttpServletRequest request) {
        KeycloakAuthenticationToken token = (KeycloakAuthenticationToken) request.getUserPrincipal();
        KeycloakPrincipal principal=(KeycloakPrincipal)token.getPrincipal();
        KeycloakSecurityContext session = principal.getKeycloakSecurityContext();
        Boolean isAdmin = session.getToken().getRealmAccess().getRoles().contains("admin");
        if (repository.existsById(session.getToken().getId())) {
            User user = repository.getOne(session.getToken().getId());
            user.setAdmin(isAdmin);
            return user;
        }
        else {
            User user = new User(request);
            user.setAdmin(isAdmin);
            repository.save(user);
            return user;
        }
    }
}
