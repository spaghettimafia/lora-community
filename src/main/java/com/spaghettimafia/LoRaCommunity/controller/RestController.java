package com.spaghettimafia.LoRaCommunity.controller;

import com.spaghettimafia.LoRaCommunity.data.entity.ForumTopic;
import com.spaghettimafia.LoRaCommunity.data.entity.WikiArticle;
import com.spaghettimafia.LoRaCommunity.data.entity.WikiCategory;
import com.spaghettimafia.LoRaCommunity.data.repository.ForumTopicRepository;
import com.spaghettimafia.LoRaCommunity.data.repository.WikiCategoryRepository;
import com.spaghettimafia.LoRaCommunity.service.ForumTopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api")
public class RestController {
    @Autowired
    private WikiCategoryRepository wikiCategoryRepository;

    @GetMapping("/all")
    public List<WikiCategory> getAll() {
        List<WikiCategory> items = wikiCategoryRepository.findAll();
        return items;
    }
}
