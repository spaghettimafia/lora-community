package com.spaghettimafia.LoRaCommunity.controller;

import com.spaghettimafia.LoRaCommunity.data.entity.ForumTopic;
import com.spaghettimafia.LoRaCommunity.data.entity.User;
import com.spaghettimafia.LoRaCommunity.data.entity.WikiArticle;
import com.spaghettimafia.LoRaCommunity.data.repository.ForumTopicRepository;
import com.spaghettimafia.LoRaCommunity.data.repository.WikiArticleRepository;
import com.spaghettimafia.LoRaCommunity.data.repository.WikiCategoryRepository;
import com.spaghettimafia.LoRaCommunity.service.UserService;
import org.keycloak.authorization.client.util.Http;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    UserService userService;
    @Autowired
    WikiCategoryRepository wikiCategoryRepository;
    @Autowired
    WikiArticleRepository wikiArticleRepository;
    @Autowired
    ForumTopicRepository forumTopicRepository;

    @RequestMapping("/wiki")
    public String wiki(HttpServletRequest request, Model model) {
        User user = userService.getUser(request);
        model.addAttribute("user", user);
        model.addAttribute("categories", wikiCategoryRepository.findAll());
        model.addAttribute("articles", wikiArticleRepository.findAll());
        return "admin_wiki";
    }

    @RequestMapping("/article")
    public String article(HttpServletRequest request, Model model, @RequestParam(value="edit", required=false) Integer article) {
        User user = userService.getUser(request);
        model.addAttribute("user", user);
        model.addAttribute("categories", wikiCategoryRepository.findAll());
        if (article != null) {
            model.addAttribute("article", wikiArticleRepository.getOne(article));
            model.addAttribute("action", "Artikel bewerken");
        }
        else {
            WikiArticle tempArticle = new WikiArticle();
            model.addAttribute("article", tempArticle);
            model.addAttribute("action", "Artikel maken");
        }
        return "admin_article";
    }

    @RequestMapping("/forum")
    public String forum(HttpServletRequest request, Model model) {
        User user = userService.getUser(request);
        model.addAttribute("user", user);
        model.addAttribute("topics", forumTopicRepository.findAll());
        return "admin_forum";
    }

    @RequestMapping("/icons")
    public String icons(HttpServletRequest request, Model model) {
        User user = userService.getUser(request);
        model.addAttribute("user", user);
        return "admin_icons";
    }
}
