package com.spaghettimafia.LoRaCommunity.controller;

import com.spaghettimafia.LoRaCommunity.data.entity.ForumPost;
import com.spaghettimafia.LoRaCommunity.data.entity.ForumReaction;
import com.spaghettimafia.LoRaCommunity.data.entity.ForumTopic;
import com.spaghettimafia.LoRaCommunity.data.entity.User;
import com.spaghettimafia.LoRaCommunity.data.repository.ForumPostRepository;
import com.spaghettimafia.LoRaCommunity.data.repository.ForumReactionRepository;
import com.spaghettimafia.LoRaCommunity.data.repository.ForumTopicRepository;
import com.spaghettimafia.LoRaCommunity.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/forum")
public class ForumController {
    @Autowired
    private ForumTopicRepository forumTopicRepository;
    @Autowired
    private ForumPostRepository forumPostRepository;
    @Autowired
    private ForumReactionRepository forumReactionRepository;
    @Autowired
    private UserService userService;


    @RequestMapping()
    public String forum(HttpServletRequest request, Model model, @RequestParam(value="post", required=false) Integer post) {
        User user = userService.getUser(request);
        model.addAttribute("user", user);
        if (post != null) {
            model.addAttribute("post", forumPostRepository.getOne(post));
            return "forum_post";
        }
        else {
            model.addAttribute("topics", forumTopicRepository.findAll());
            return "forum";
        }
    }

    @RequestMapping("/like")
    @ResponseBody
    public String like(@RequestParam(value="reaction", required=false) Integer reaction,
                               @RequestParam(value="post", required=false) Integer post) {
        String likes = "None";
        if (reaction != null) {
            ForumReaction tempReaction = forumReactionRepository.getOne(reaction);
            tempReaction.setLikes(tempReaction.getLikes() + 1);
            likes = tempReaction.getLikes().toString();
            forumReactionRepository.save(tempReaction);
        }
        if (post != null) {
            ForumPost tempPost = forumPostRepository.getOne(post);
            tempPost.setLikes(tempPost.getLikes() + 1);
            likes = tempPost.getLikes().toString();
            forumPostRepository.save(tempPost);
        }
        return likes;
    }

    @PostMapping("/post/{topic}/save")
    @ResponseBody
    public Boolean save_post(HttpServletRequest request, @RequestBody ForumPost post, @PathVariable Integer topic) {
        post.setTopic(forumTopicRepository.getOne(topic));
        post.setUser(userService.getUser(request));
        post.setDate();
        forumPostRepository.save(post);
        return true;
    }

    @PostMapping("/reaction/{post}/save")
    @ResponseBody
    public Boolean save_reaction(HttpServletRequest request, @RequestBody ForumReaction reaction, @PathVariable Integer post) {
        reaction.setPost(forumPostRepository.getOne(post));
        reaction.setUser(userService.getUser(request));
        reaction.setDate();
        forumReactionRepository.save(reaction);
        return true;
    }

    @PostMapping("/topic/save")
    @ResponseBody
    public Boolean save_topic(@RequestBody ForumTopic topic) {
        forumTopicRepository.save(topic);
        return true;
    }

    @PostMapping("/topic/{id}/delete")
    @ResponseBody
    public Boolean delete_topic(@PathVariable Integer id) {
        forumTopicRepository.deleteById(id);
        return true;
    }

    @PostMapping("/post/{id}/delete")
    @ResponseBody
    public Boolean delete_post(@PathVariable Integer id) {
        forumPostRepository.deleteById(id);
        return true;
    }

    @PostMapping("/reaction/{id}/delete")
    @ResponseBody
    public Boolean delete_reaction(@PathVariable Integer id) {
        forumReactionRepository.deleteById(id);
        return true;
    }
}
