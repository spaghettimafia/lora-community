package com.spaghettimafia.LoRaCommunity.controller;

import com.spaghettimafia.LoRaCommunity.data.entity.User;
import com.spaghettimafia.LoRaCommunity.data.entity.WikiArticle;
import com.spaghettimafia.LoRaCommunity.data.entity.WikiCategory;
import com.spaghettimafia.LoRaCommunity.data.repository.WikiArticleRepository;
import com.spaghettimafia.LoRaCommunity.data.repository.WikiCategoryRepository;
import com.spaghettimafia.LoRaCommunity.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/wiki")
public class WikiController {
    @Autowired
    private WikiCategoryRepository wikiCategoryRepository;
    @Autowired
    private WikiArticleRepository wikiArticleRepository;
    @Autowired
    UserService userService;

    @RequestMapping
    public String wiki(HttpServletRequest request, Model model, @RequestParam(value="article", required=false) Integer article) {
        User user = userService.getUser(request);
        model.addAttribute("user", user);
        if (article != null) {
            model.addAttribute("article", wikiArticleRepository.getOne(article));
            return "wiki_article";
        }
        else {
            List<WikiCategory> categories = wikiCategoryRepository.findAll();
            model.addAttribute("categories", categories);
            return "wiki";
        }
    }

    @RequestMapping("/like")
    @ResponseBody
    public String like(@RequestParam(value="article") Integer id) {
        WikiArticle tempArticle = wikiArticleRepository.getOne(id);
        tempArticle.setHelpful(tempArticle.getHelpful() + 1);
        String likes = tempArticle.getHelpful().toString();
        wikiArticleRepository.save(tempArticle);
        return likes;
    }

    @PostMapping("/category/save")
    @ResponseBody
    public Boolean save_category(@RequestBody WikiCategory category) {
        wikiCategoryRepository.save(category);
        return true;
    }

    @PostMapping("/category/{id}/delete")
    @ResponseBody
    public Boolean delete_category(@PathVariable Integer id) {
        wikiCategoryRepository.deleteById(id);
        return true;
    }

    @PostMapping("/article/{category}/save")
    @ResponseBody
    public Boolean save_article(@RequestBody WikiArticle article, @PathVariable Integer category) {
        article.setDate();
        article.setCategory(wikiCategoryRepository.getOne(category));
        wikiArticleRepository.save(article);
        return true;
    }

    @PostMapping("/article/{id}/delete")
    @ResponseBody
    public Boolean delete_article(@PathVariable Integer id) {
        wikiArticleRepository.deleteById(id);
        return true;
    }
}
