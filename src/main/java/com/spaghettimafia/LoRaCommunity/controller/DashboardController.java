package com.spaghettimafia.LoRaCommunity.controller;

import com.spaghettimafia.LoRaCommunity.data.entity.User;
import com.spaghettimafia.LoRaCommunity.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class DashboardController {
    @Autowired
    UserService userService;


    @RequestMapping("/home")
    public String home(HttpServletRequest request, Model model) {
        User user = userService.getUser(request);
        model.addAttribute("user", user);
        return "home";
    }

    @RequestMapping("/about")
    public String about(HttpServletRequest request, Model model) {
        User user = userService.getUser(request);
        model.addAttribute("user", user);
        return "about";
    }

    @RequestMapping("/project")
    public String project(HttpServletRequest request, Model model) {
        User user = userService.getUser(request);
        model.addAttribute("user", user);
        return "project";
    }

    @RequestMapping("/projects")
    public String projects(HttpServletRequest request, Model model) {
        User user = userService.getUser(request);
        model.addAttribute("user", user);
        return "projects";
    }

    @RequestMapping("/profile")
    public String profile(HttpServletRequest request, Model model) {
        User user = userService.getUser(request);
        model.addAttribute("user", user);
        return "profile";
    }

    @RequestMapping("/ping")
    public ResponseEntity ping() {
        return ResponseEntity.ok().build();
    }
}
