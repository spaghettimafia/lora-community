package com.spaghettimafia.LoRaCommunity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoRaCommunityApplication {

	public static void main(String[] args) {
		SpringApplication.run(LoRaCommunityApplication.class, args);
	}

}