FROM openjdk:latest
VOLUME /tmp
ADD /target/*.jar app.jar
EXPOSE 8443
ENTRYPOINT ["java", "-jar", "/app.jar", "--spring.profiles.active=production"]