# LoRa Community
![Build](https://gitlab.com/spaghettimafia/lora-community/badges/master/pipeline.svg)

Wij zijn groep 27 en voor het lectoraat Data Intelligence gaan wij aan de slag met het ontwikkelen van een community voor een LoRa netwerk. In deze community kunnen studenten, docenten en bedrijven samenkomen en werken aan hun projecten.

### Componenten
Deze repository is enkel het dashboard voor de community. De gehele community bestaat uit diverse componenten. Deze componenten worden gehost in Microsoft Azure.
- Authenticatieserver (Keycloak)
- Repository management met versiebeheer (Gitlab)
- Database (Microsoft SQL Server)
- Community dashboard (Springboot/Java)

### Live demo
De live versie is te bezoeken via onderstaande link:
[Community Dashboard](https://lora.ipictserver.nl)

#### Inloggen
**Gebruiker:** `user` `demo`